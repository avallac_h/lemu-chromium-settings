#!/usr/bin/env python3

import codecs
import copy
import json
import sys

from pathlib import Path

import yaml


def deepmerge(dst: dict, src: dict, sort=False, uniq=False) -> dict:
    """Recursively merge two dictionaries along with nested dicts.

    Based on the deepupdate() function by Ferry Boender:
    <https://www.electricmonk.nl/log/2017/05/07/merging-two-python-dictionaries-by-deep-updating/>

    Args:
        dst: Dictionary to merge into.
        src: Dictionary to merge from.
        sort: Sort nested lists.
        uniq: Uniquify nested lists (order preserving).

    Returns:
        Merged dictionary.

    """
    assert isinstance(dst, dict)
    assert isinstance(src, dict)

    def uniq_f8(seq: list) -> list:
        """Uniquify list (order preserving).

        This is f8() function by Dave Kirby:
        <https://www.peterbe.com/plog/fastest-way-to-uniquify-a-list-in-python-3.6>
        """
        seen = set()
        return [x for x in seq if x not in seen and not seen.add(x)]  # type: ignore[func-returns-value]

    for key, val in src.items():
        if isinstance(val, dict):
            if key not in dst:
                dst[key] = copy.deepcopy(val)
            else:
                deepmerge(dst[key], val, sort=sort, uniq=uniq)

        elif isinstance(val, list):
            if key not in dst:
                dst[key] = copy.deepcopy(val)
            else:
                dst[key].extend(val)
                if sort:
                    dst[key].sort()
                if uniq:
                    dst[key] = uniq_f8(dst[key])

        else:
            dst[key] = copy.copy(val)

    return dst


def dict2reg(data: dict) -> list:
    # <https://dev.chromium.org/administrators/complex-policies-on-windows>
    assert isinstance(data, dict)

    lines = []

    for key, val in data.items():
        if isinstance(val, bool) or isinstance(val, int):
            lines.append('"{}"=dword:{:08d}'.format(key, int(val)))
        elif isinstance(val, str):
            lines.append('"{}"="{}"'.format(key, val.replace('"', '\\"')))
        else:
            lines.append(
                '"{}"="{}"'.format(
                    key,
                    json.dumps(val, ensure_ascii=False).replace('"', '\\"'),
                )
            )

    return lines


def main():
    builddir = Path(__file__).parent / "chromium"
    mprefs = builddir / "master_preferences"

    yaml_files = list(builddir.rglob("*.y*ml"))
    json_files = list(builddir.rglob("*.json"))
    reg_files = list(builddir.rglob("*.reg"))

    yaml_files.sort()
    json_files.sort()
    reg_files.sort()

    if mprefs.is_file():
        json_files.append(mprefs)

    # Clean generated REG and JSON files
    if json_files or reg_files:
        sys.stderr.write("==> Cleaning...\n")

        for fpath in json_files + reg_files:
            sys.stderr.write("rm {}\n".format(fpath))
            fpath.unlink()

    # Generate JSON files from YAML
    if yaml_files:
        sys.stderr.write("==> Generating JSON policies...\n")

        for yaml_file in yaml_files:
            json_file = yaml_file.with_suffix(".json")
            sys.stderr.write("{} -> {}\n".format(yaml_file, json_file))

            with open(yaml_file, "r") as yaml_fh:
                with open(json_file, "w") as json_fh:
                    json.dump(
                        yaml.load(yaml_fh, Loader=yaml.SafeLoader),
                        json_fh,
                        indent=4,
                    )

    # Generate REG files from YAML
    if yaml_files:
        sys.stderr.write("==> Generating REG policies...\n")

        regs_content = {"managed": [], "recommended": []}

        for yaml_file in yaml_files:
            policy_type = yaml_file.parent.name
            if policy_type not in regs_content.keys():
                continue

            sys.stderr.write("r {}\n".format(yaml_file))

            with open(yaml_file, "r") as yaml_fh:
                # header
                regs_content[policy_type].append(";")
                regs_content[policy_type].append(
                    '; From "{}\\{}" '.format(
                        policy_type, yaml_file.with_suffix("").name
                    ).ljust(72, "-")
                )

                # data
                regs_content[policy_type].extend(
                    dict2reg(yaml.load(yaml_fh, Loader=yaml.SafeLoader))
                )

        # write reg files
        for policy_type in regs_content.keys():
            reg_file = builddir / "hklm_{}.reg".format(policy_type)
            sys.stderr.write("w {}\n".format(reg_file))

            reg_key = (
                "HKEY_LOCAL_MACHINE\\SOFTWARE\\Policies\\Google\\Chrome"
            )
            if policy_type == "recommended":
                reg_key += "\\Recommended"

            with open(
                reg_file, "w", encoding="utf-16-le", newline="\r\n"
            ) as reg_fh:
                reg_fh.write(codecs.BOM_UTF16_LE.decode("utf-16-le"))
                reg_fh.write("Windows Registry Editor Version 5.00\n\n")
                reg_fh.write(";" + "=" * 71 + "\n")
                reg_fh.write(
                    "; !!! WARNING !!! "
                    "PLEASE, DO NOT EDIT THIS FILE USING WINDOWS NOTEPAD\n"
                    ";                 "
                    "IT CANNOT HANDLE LINES LONGER THAN 1024 CHARACTERS\n"
                )
                reg_fh.write(";" + "=" * 71 + "\n;\n")
                reg_fh.write("[{}]\n".format(reg_key))
                reg_fh.write("\n".join(regs_content[policy_type]))

    # Join master preferences
    sys.stderr.write("==> Generating master_preferences...\n")

    mpref_files = list((builddir / "master_preferences.d").glob("*.json"))
    mprefs_data = {}

    for mpref_file in mpref_files:
        sys.stderr.write("r {}\n".format(mpref_file))
        with open(mpref_file, "r") as mpref_fh:
            mprefs_data = deepmerge(
                mprefs_data, json.load(mpref_fh), sort=True, uniq=True
            )

    sys.stderr.write("w {}\n".format(mprefs))
    with open(mprefs, "w") as mpref_fh:
        json.dump(mprefs_data, mpref_fh, indent=4)


if __name__ == "__main__":
    main()
