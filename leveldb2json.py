#!/usr/bin/env python3

import sys

import plyvel


def main():
    try:
        db = plyvel.DB(sys.argv[1])
    except Exception as e:
        print(
            "Cannot open db or missing argument: %s" % e,
            file=sys.stderr,
        )
        sys.exit(1)

    lines = []

    for k, v in db:
        if v in ["true", "false"]:
            lines.append(
                '{i}"{k}": {v}'.format(
                    i=" " * 4, k=k.decode(), v=v.decode()
                )
            )
        else:
            lines.append(
                '{i}"{k}": {v}'.format(
                    i=" " * 4, k=k.decode(), v=v.decode()
                )
            )

    print("{\n" + ",\n".join(lines) + "\n}")


if __name__ == "__main__":
    main()
