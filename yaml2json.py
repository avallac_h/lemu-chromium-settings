#!/usr/bin/env python3
"""
https://github.com/redsymbol/json2yaml
"""

import sys
import json
import yaml
import argparse


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('yaml_file', nargs='?', type=argparse.FileType('r'), default=sys.stdin)

    yaml_file = parser.parse_args().yaml_file
    yaml_body = yaml.load(yaml_file, Loader=yaml.SafeLoader)
    json_body = json.dump(yaml_body, sys.stdout, indent=4)
    sys.stdout.write('\n')


if __name__ == "__main__":
    main()
