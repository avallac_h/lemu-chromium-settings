#!/bin/sh
#===========================================================================
# Chromium settings install script
#
# This script should work on bash, zsh, busybox sh, ash, dash and ksh.
# Tested on:
#  * arch 2020.09 + bash 5.0.018 + chromium 85.0.4183
#  * arch 2020.09 + busybox sh 1.31.1 + chromium 85.0.4183
#  * ubuntu 18.04.4 + dash 0.5.8 + chromium 85.0.4183
#  * openbsd 6.7 + ksh 5.2.14 + chromium 81.0.4044
#  * oracle linux 8.3 + bash 4.4.19 + chromium 87.0.4280.88
#===========================================================================
set -e

# Set default variables ----------------------------------------------------
DBG=0
CURRENT_TIME=$(date "+%Y%m%d_%H%M%S")
OS_NAME=$(uname -s | awk '{print tolower($0)}')
OS_DIST=
OS_REL=
OS_CODE=

if [ "$OS_NAME" = "linux" ] ; then
    if command -v lsb_release >/dev/null 2>&1 ; then
        OS_DIST=$(lsb_release -i -s | awk '{ print tolower($1) }')
        OS_REL=$(lsb_release -r -s | awk '{ print tolower($1) }')
        OS_CODE=$(lsb_release -c -s | awk '{ print tolower($1) }')

    elif [ -f /etc/os-release ] ; then
        OS_DIST=$(
            awk -F "=" '/^ID=/ { gsub("\"",""); print tolower($2) }' \
            /etc/os-release
        )
        OS_REL=$(
            awk -F "=" '/^VERSION_ID=/ { gsub("\"",""); print tolower($2) }' \
            /etc/os-release
        )
        OS_CODE=$(
            awk -F "=" '/^VERSION_CODENAME=/ { gsub("\"",""); print tolower($2) }' \
            /etc/os-release
        )
    fi

elif [ "$OS_NAME" = "openbsd" ] ; then
    OS_REL=$(uname -r)
fi

if [ $DBG -gt 0 ] ; then
    cat << EOF
+----------------+
| OS Information |
+----------------+
OS_NAME         = $OS_NAME
OS_DIST         = $OS_DIST
OS_REL          = $OS_REL
OS_CODE         = $OS_CODE
EOF
fi

if [ "$OS_NAME" = "linux" ] ; then
    case $OS_DIST in
        debian) 
                case $OS_CODE in
                    stretch)
                        DEFAULT_POLICIES_DIR="/etc/chromium/policies"
                        DEFAULT_MPREFS_PATH="/usr/share/chromium/master_preferences" # [1_]
                        ;;
                    *)
                        DEFAULT_POLICIES_DIR="/etc/chromium/policies"
                        DEFAULT_MPREFS_PATH="/etc/chromium/master_preferences" # [2_] [3_]
                        ;;
                esac
                ;;
        ubuntu) 
                if [ "$(echo "$OS_REL" 19.10 | awk '{ if ($1 > $2) print 1 }')" = 1 ] ; then
                    # snap package [4_] [5_]
                    DEFAULT_POLICIES_DIR="/var/snap/chromium/current/policies"
                    DEFAULT_MPREFS_PATH="/var/snap/chromium/current/master_preferences"
                else
                    # deb package
                    DEFAULT_POLICIES_DIR="/etc/chromium-browser/policies"
                    DEFAULT_MPREFS_PATH="/usr/lib/chromium-browser/master_preferences"
                fi
                ;;
        centos|ol|oracle*)
                DEFAULT_POLICIES_DIR="/etc/chromium/policies"
                DEFAULT_MPREFS_PATH="/etc/chromium/master_preferences"
                ;;
        *)
                DEFAULT_POLICIES_DIR="/etc/chromium/policies"
                DEFAULT_MPREFS_PATH="/usr/lib/chromium/master_preferences"
                ;;
    esac

elif [ "$OS_NAME" = "openbsd" ] ; then
    DEFAULT_POLICIES_DIR="/etc/chromium/policies"
    DEFAULT_MPREFS_PATH="/usr/local/chrome/master_preferences"

else
    DEFAULT_POLICIES_DIR="/etc/chromium/policies"
    DEFAULT_MPREFS_PATH="/usr/lib/chromium/master_preferences"
fi

DEST_DIR=
POLICIES_DIR=$DEFAULT_POLICIES_DIR
MPREFS_PATH=$DEFAULT_MPREFS_PATH
NO_BACKUP=0
WITH_YAML=0


# Functions ----------------------------------------------------------------
usage() {
cat <<EOF
Usage:
  $(basename "$0") [-hny] [-d DEST_DIR] [-p POLICIES_DIR] [-m MPREFS_PATH] 
Options:
  -h                  show this help information
  -d DEST_DIR         destination directory (used as prefix for -p and -e)
  -p POLICIES_DIR     policies directory (def.: $DEFAULT_POLICIES_DIR)
  -m MPREFS_PATH      master_preferences path (def.: $DEFAULT_MPREFS_PATH)
  -n                  don't backup and clean existing files
  -y                  copy source yaml files in addition to json files

EOF
}


# Parse and verify options -------------------------------------------------
OPTERR=1
OPTIND=1
while getopts hnyd:p:m: opt ; do
    case $opt in
        h)  usage
            exit 0
            ;;
        d)  DEST_DIR=$OPTARG
            ;;
        p)  POLICIES_DIR=$OPTARG
            ;;
        m)  MPREFS_PATH=$OPTARG
            ;;
        n)  NO_BACKUP=1
            ;;
        y)  WITH_YAML=1
            ;;
        \?) usage >&2
            exit 1
            ;;
    esac
done
shift "$((OPTIND-1))"

# Check that there are no arguments left
if [ -n "$1" ] ; then
    printf "%s: illegal argument -- %s\n" "$0" "$1" >&2
    usage >&2
    exit 1
fi

# Check that none of the $OPTARGs start with a '-'
if [ "$(echo "x$DEST_DIR" | cut -c 2)" = "-" ] ; then
    printf "%s: invalid argument for option -d -- %s\n" \
        "$0" "$DEST_DIR" >&2
    usage >&2
    exit 1

elif [ "$(echo "x$POLICIES_DIR" | cut -c 2)" = "-" ] ; then
    printf "%s: invalid argument for option -p -- %s\n" \
        "$0" "$POLICIES_DIR" >&2
    usage >&2
    exit 1

elif [ "$(echo "x$MPREFS_PATH" | cut -c 2)" = "-" ] ; then
    printf "%s: invalid argument for option -e -- %s\n" \
        "$0" "$MPREFS_PATH" >&2
    usage >&2
    exit 1
fi

# Prepend DEST_DIR and clean the paths 
if [ -n "$DEST_DIR" ] ; then
    POLICIES_DIR=$(echo "$DEST_DIR/$POLICIES_DIR" \
        | sed -e 's|//|/|g' -e 's|/$||')
    MPREFS_PATH=$(echo "$DEST_DIR/$MPREFS_PATH" \
        | sed -e 's|//|/|g')
fi

if [ $DBG -gt 0 ] ; then
    cat << EOF
+----------------------+
| Options and switches |
+----------------------+
DEST_DIR        = $DEST_DIR
POLICIES_DIR    = $POLICIES_DIR
MPREFS_PATH     = $MPREFS_PATH
NO_BACKUP       = $NO_BACKUP
WITH_YAML       = $WITH_YAML
EOF
fi


# Perform backup------------------------------------------------------------
if [ $NO_BACKUP -eq 0 ] ; then
    if [ -d "$POLICIES_DIR" ] ; then
        echo "==> Backing up previous policies..." >&2
        echo "'$POLICIES_DIR' -> '${POLICIES_DIR}.${CURRENT_TIME}'"
        mv "$POLICIES_DIR" "${POLICIES_DIR}.${CURRENT_TIME}"
    fi
    if [ -f "$MPREFS_PATH" ] ; then
        echo "==> Backing up previous master_preferences file..." >&2
        echo "'$MPREFS_PATH' -> '${MPREFS_PATH}.${CURRENT_TIME}'"
        mv "$MPREFS_PATH" "${MPREFS_PATH}.${CURRENT_TIME}"
    fi
fi


# Installing files ---------------------------------------------------------
echo "==> Installing policies..." >&2

for POLICY_TYPE in managed recommended ; do
    echo "'$POLICIES_DIR/$POLICY_TYPE'"
    mkdir -p "$POLICIES_DIR/$POLICY_TYPE"
    
    if [ $WITH_YAML -eq 1 ] ; then
        find "$(dirname "$0")/chromium/policies/$POLICY_TYPE"   \
            -type f \( -iname "*.json" -o -iname "*.y*ml" \)    \
            -print                                              \
            -exec cp "{}" "$POLICIES_DIR/$POLICY_TYPE/" \;
    else
        find "$(dirname "$0")/chromium/policies/$POLICY_TYPE"   \
            -type f -iname "*.json"                             \
            -print                                              \
            -exec cp "{}" "$POLICIES_DIR/$POLICY_TYPE/" \;
    fi
done

echo "==> Installing master_preferences..." >&2
echo "'$(dirname "$MPREFS_PATH")'"
mkdir -p "$(dirname "$MPREFS_PATH")"
echo "'$(dirname "$0")/chromium/master_preferences' -> '$MPREFS_PATH'"
cp "$(dirname "$0")/chromium/master_preferences" "$MPREFS_PATH"

echo "All done." >&2

# [1_]: <https://sources.debian.org/src/chromium/73.0.3683.75-1~deb9u1/debian/patches/debianization/master-preferences.patch/>
# [2_]: <https://sources.debian.org/src/chromium-browser/57.0.2987.98-1~deb8u1/debian/patches/prefs.patch/>
# [3_]: <https://sources.debian.org/src/chromium/83.0.4103.116-1~deb10u3/debian/patches/debianization/master-preferences.patch/>
# [4_]: <https://bugs.launchpad.net/ubuntu/+source/chromium-browser/+bug/1714244>
# [5_]: <https://bugs.launchpad.net/ubuntu/+source/chromium-browser/+bug/1866732>
