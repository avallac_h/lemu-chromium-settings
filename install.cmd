@echo off

:: Get current date and time (locale independent)
for /F "usebackq tokens=1,2 delims==" %%i ^
in (`wmic os get LocalDateTime /VALUE 2^>NUL`) ^
do if '.%%i.'=='.LocalDateTime.' set CURRENT_TIME=%%j

set CURRENT_TIME=%CURRENT_TIME:~0,8%_%CURRENT_TIME:~8,6%

:: Set variables
set "BACKUP_DIR=%USERPROFILE%\Backup\Chrome Settings"
set "MPREFS_DIR=%PROGRAMFILES%\Google\Chrome\Application"

:: Backup
echo ==^> Creating backup directory...
echo "%BACKUP_DIR%"
mkdir "%BACKUP_DIR%"
echo.

:backup
    echo ==^> Backing up previous policies...
    echo "HKLM\SOFTWARE\Policies\Google\Chrome" -^> "%BACKUP_DIR%\policies.%CURRENT_TIME%.reg"
    reg export "HKLM\SOFTWARE\Policies\Google\Chrome" "%BACKUP_DIR%\policies.%CURRENT_TIME%.reg"
    echo.
    if not exist "%MPREFS_DIR%\master_preferences" goto install

    echo ==^> Backing up previous master_preferences file...
    echo "%MPREFS_DIR%\master_preferences" -^> "%BACKUP_DIR%\master_preferences.%CURRENT_TIME%.reg"
    copy /y "%MPREFS_DIR%\master_preferences" "%BACKUP_DIR%\master_preferences.%CURRENT_TIME%.reg" || goto err
    echo.

:install
    echo ==^> Installing policies...
    echo "%~dp0\chromium\hklm_managed.reg"
    regedit /s "%~dp0\chromium\hklm_managed.reg" || goto err
    echo "%~dp0\chromium\hklm_recommended.reg"
    regedit /s "%~dp0\chromium\hklm_recommended.reg" || goto err
    echo.

    echo ==^> Installing master_preferences...
    echo "%MPREFS_DIR%"
    mkdir "%MPREFS_DIR%"
    echo "%~dp0\chromium\master_preferences" -^> "%MPREFS_DIR%\master_preferences"
    copy /y "%~dp0\chromium\master_preferences" "%MPREFS_DIR%\master_preferences" || goto err
    echo.
    goto ok


:: Exit labels
:ok
    echo All done.
    pause
    exit /b 0

:err
    set RETVAL=%ERRORLEVEL%
    echo ERROR: program returned non-zero exitcode: %RETVAL%
    pause
    exit /b %RETVAL%

:eof
::  vim: set syntax=dosbatch ts=4 sts=4 sw=4 tw=0 et :
