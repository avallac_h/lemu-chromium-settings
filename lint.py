#!/usr/bin/env python3

import argparse
import json
import sys
from pathlib import Path
from typing import Dict

import yaml
from colorama import init, Fore, Style


class Linter:
    def __init__(self, templates_file: Path):
        # vars
        self.definitions: Dict[str, dict] = {}

        self.cur_filepath = Path()
        self.cur_policy_name = ""

        self.processed_policies: Dict[str, list] = {}

        self.total_errors = 0
        self.total_warnings = 0

        # init colorama
        init()

        # read policy definitions from the templates file
        templates = eval(templates_file.open("r", encoding="utf-8").read())

        for policy in templates["policy_definitions"]:
            if ["type"] == "group":
                continue
            self.definitions[policy["name"]] = policy

    def _report(self, mark: str, template: str, *args) -> None:
        print(
            (
                "{m}{mark}: {filename}:{r} {p}{policy}:{r} " + template
            ).format(
                m=Fore.YELLOW if mark == "W" else Fore.RED,
                mark=mark,
                p=Fore.CYAN,
                r=Style.RESET_ALL,
                filename=self.cur_filepath,
                policy=self.cur_policy_name or "N/A",
                *args
            )
        )

    def warn(self, template: str, *args) -> None:
        self.total_warnings += 1
        self._report("W", template, *args)

    def error(self, template: str, *args) -> None:
        self.total_errors += 1
        self._report("E", template, *args)

    def print_totals(self):
        print(
            "{s}TOTAL: errors: {e}, warnings: {w}.{r}".format(
                s=Style.BRIGHT,
                e=self.total_errors,
                w=self.total_warnings,
                r=Style.RESET_ALL,
            ),
            file=sys.stderr,
        )

    def check_file(self, filepath: Path):
        # Set self.cur_policy_name used by warn() and error() methods
        self.cur_filepath = filepath

        with filepath.open() as fp:
            policies_from_file = yaml.load(fp, Loader=yaml.SafeLoader)

        # ------------------------------------------------------------------
        # [W]: No policies found. Empty file?
        if policies_from_file is None:
            self.warn("No policies found. Empty file?")

        # ------------------------------------------------------------------
        # [E]: Wrong YAML structure: not a dictionary
        elif not isinstance(policies_from_file, dict):
            self.error("Wrong YAML structure: not a dictionary")

        else:
            for policy_name, policy_value in policies_from_file.items():
                self._check_policy(policy_name, policy_value)

    def _check_policy(self, policy_name: str, policy_value) -> None:
        self.cur_policy_name = policy_name

        # ------------------------------------------------------------------
        # [W]: Policy was already defined in: (filelist)
        if self.processed_policies.get(policy_name):
            self.warn(
                "Policy was already defined in: {}",
                ", ".join(self.processed_policies[policy_name]),
            )
            self.processed_policies[policy_name].append(
                str(self.cur_filepath)
            )
        else:
            self.processed_policies[policy_name] = [str(self.cur_filepath)]

        # ------------------------------------------------------------------
        # [W]: Unknown policy
        if not self.definitions.get(policy_name):
            self.warn("Unknown policy")

            # Reset self.cur_policy_name used by warn() and error() methods
            self.cur_policy_name = ""

            return

        # ------------------------------------------------------------------
        # Simplify supported_on list (get only desktop browser info)
        policy_supported_on = {}
        for platform in self.definitions[policy_name]["supported_on"]:
            if platform.startswith("chrome."):
                tos, ver = platform.replace("chrome.", "").split(":")
                tos = tos[:3]
                if tos == "*":
                    for key in ["win", "mac", "lin"]:
                        policy_supported_on[key] = ver
                else:
                    policy_supported_on[tos] = ver

        # ------------------------------------------------------------------
        # [E]: Policy cannot be recommended
        # NOTE: split() is useful in case of fontconfig-like configuration
        #       with symlinks:
        #           "recommended.avail/mypolicy" -> "recommended/mypolicy"
        policy_parent_dir = self.cur_filepath.parent.name.lower().split(
            "."
        )[0]

        if policy_parent_dir == "recommended" and not self.definitions[
            policy_name
        ]["features"].get("can_be_recommended"):
            self.error("Policy cannot be recommended")

        # ------------------------------------------------------------------
        # [W]: Policy flagged as deprecated
        if self.definitions[policy_name].get("deprecated"):
            self.warn(
                "Policy flagged as deprecated (supported on {})",
                ", ".join(map(":".join, policy_supported_on.items())),
            )

        # ------------------------------------------------------------------
        # [W]: Policy doesn't apply to desktop browser
        if not policy_supported_on:
            self.warn("Policy doesn't apply to desktop browser")

        # ------------------------------------------------------------------
        # [E]: Wrong value type: "X" instead of "Y"
        # [E]: Invalid value: (JSON error)
        type_trans = {
            "array": "list",
            "boolean": "bool",
            "integer": "int",
            "string": "str",
            "object": "str",
        }

        expected_value_type = self.definitions[policy_name]["schema"][
            "type"
        ]

        if type(policy_value).__name__ == type_trans.get(
            expected_value_type
        ):
            if expected_value_type == "object":
                try:
                    json.loads(policy_value)
                except ValueError as e:
                    self.error("Invalid value: {}", e)

        else:
            self.error(
                'Wrong value data type: "{}" instead of "{}"',
                type(policy_value).__name__,
                type_trans.get(expected_value_type),
            )

        # Reset self.cur_policy_name used by warn() and error() methods
        self.cur_policy_name = ""


def main():
    # Defaults
    default_templates_file = (
        Path(__file__).parent / "3rd-party" / "policy_templates.json"
    )
    default_policies_dir = Path(__file__).parent / "chromium" / "policies"

    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-t",
        "--templates-file",
        metavar="policy_templates.json",
        type=Path,
        default=default_templates_file,
        help="use specified policy templates file (def.: {})".format(
            default_templates_file
        ),
    )
    parser.add_argument(
        "-d",
        "--policies-dir",
        metavar="DIR",
        type=Path,
        default=default_policies_dir,
        help=(
            'directory to lint, should contain "managed" and '
            + '"recommended" subdirs (def.: {})'.format(
                default_policies_dir
            )
        ),
    )
    args = parser.parse_args()

    # Init linter with specified policy templates file
    linter = Linter(args.templates_file)

    # Check YAML files
    yaml_files = sorted(
        list((args.policies_dir / "managed").glob("*.y*ml"))
        + list((args.policies_dir / "recommended").glob("*.y*ml"))
    )
    for yaml_file in yaml_files:
        linter.check_file(yaml_file)

    # Print totals and set return value
    linter.print_totals()
    if linter.total_errors:
        sys.exit(1)


if __name__ == "__main__":
    main()
