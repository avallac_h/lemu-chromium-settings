======================
LeMU Chromium Settings
======================

Custom Google Chrome/Chromium configuration


Aims
----

* **turn on most of process- or site-isolation features**

  * *site per process, all sorts of sandboxing, etc.*

* **turn off features that can affect host machine**

  * *desktop environment integration, setting Chrome as default browser, etc.*

* **turn off features that can access real hardware or local network** [#]_

  * *camera and mic. access, screen sharing, media routing, WebUSB and WebSerial
    APIs, all that stuff*

* **turn off features that can violate privacy, but not at the cost of security**

  * *this is why Safe browsing is still enabled by default, but online
    spellcheck is not*

  * *search suggestions in address bar are also disabled* [#]_

* **get rid of all those annoying popups and questions**

  * *translate popups, site notification popups, desktop notifications, etc.*


Dependencies
------------

* PyYAML:
  `[Home] <https://pyyaml.org/>`__
  `[PyPi] <https://pypi.org/project/PyYAML/>`__
  `[GitHub] <https://github.com/yaml/pyyaml/>`__

* Colorama:
  `[Home] <https://github.com/tartley/colorama>`__
  `[PyPi] <https://pypi.python.org/pypi/colorama>`__
  `[GitHub] <https://github.com/tartley/colorama>`__


Usage
-----
.. warning::
   Ubuntu maintainers shoot themselves in the foot.
   There's no adequate way to configure Chromium at the system level in Ubuntu
   19.10+. The problem is that chromium-browser now ships as snap package.
   The progress of this story can be observed here: `#1714244`_, `#1866732`_.

.. _`#1714244`: https://bugs.launchpad.net/ubuntu/+source/chromium-browser/+bug/1714244
.. _`#1866732`: https://bugs.launchpad.net/ubuntu/+source/chromium-browser/+bug/1866732


#. [optional] update policy definitions::

    $ cd ./3rd-party && wget -Ni policy_templates.json.link

#. [optional] modify ``*.yml`` files in ``chromium/policies/`` or
   ``chromium/master_preferences.d/`` according to your needs

#. [optional] use ``./lint.py`` to verify the configuration changes

#. [optional] rebuild all ``*.json``, ``*.reg`` and ``*.plist`` files using
   ``./build.py``

#. use `./install.sh` or `.\\install.cmd` [#]_ to install policies and
   `master_preferences` file

    .. warning::
       `install.sh` script runs silenty, so you should verify default paths on
       help screen (`-h || --help` before running them without arguments).

       `install.cmd` uses hardcoded paths in order to match default Google
       Chrome installation.

#. check how configuration applies to your browser using `<chrome://policy>`_
   and `<chrome://management/>`_ pages.


Notes on settings that left changeable
--------------------------------------

**Safe Browsing**:
  is actually a good feature, but in some rare cases (TOR, VPN) you might want to turn it
  off.

**History, password manager and persistent cookies**:
  left changeable because you might want to use Site-Specific Browser feature with
  different profile than your default one::

  $ chromium --user-data-dir=~/.config/com.google.android.apps.youtube.music --app=http://music.youtube.com

  You can even invent some sort of launcher on top of `Firejail`_ and/or
  encrypted fs, like `EncFS`_.

.. _`Firejail`: https://github.com/netblue30/firejail
.. _`EncFS`: https://github.com/vgough/encfs


Notes on Chrome/Chromium configuration
--------------------------------------

* `Chromium Documentation for Administrators`_ -- a useful starting point.

* `Chrome Browser quick start (Windows)`_

* `Chrome Browser for enterprise downloads (Windows, OS X)`_

.. _`Chromium Documentation for Administrators`: https://www.chromium.org/administrators
.. _`Chrome Browser quick start (Windows)`: https://support.google.com/chrome/a/answer/9023663
.. _`Chrome Browser for enterprise downloads (Windows, OS X)`: https://chromeenterprise.google/browser/download/

Group policies
^^^^^^^^^^^^^^

* `Understand Chrome policy management`_ -- a brief intro.

* `Set up Chrome Browser Cloud Management`_ -- ?.

* `Chrome Enterprise policy list`_ -- complete list of all Chrome policies.
  
  * Note, that you can jump to policy description using this link template::
  
        https://cloud.google.com/docs/chrome-enterprise/policies/?policy=POLICY_NAME

    just replace ``POLICY_NAME`` with the name of policy you are interested in.

* `Google Chrome Group Policies @ getadmx.com`_ -- the same list in a windows-aproach

* Python dictionary used to generate most of the documents above:

  * `<https://github.com/chromium/chromium/raw/master/components/policy/resources/policy_templates.json>`_

* `Descriptions found in Chromium sources`_

.. _`Understand Chrome policy management`: https://support.google.com/chrome/a/answer/9037717?hl=en
.. _`Set up Chrome Browser Cloud Management`: https://support.google.com/chrome/a/answer/9116814?hl=en
.. _`Chrome Enterprise policy list`: https://cloud.google.com/docs/chrome-enterprise/policies
.. _`Google Chrome Group Policies @ getadmx.com`: https://getadmx.com/?Category=Chrome
.. _`Descriptions found in Chromium sources`: https://source.chromium.org/chromium/chromium/src/+/master:out/Debug/gen/components/policy/proto/chrome_settings.proto


Master preferences
^^^^^^^^^^^^^^^^^^

* `<chrome://prefs-internals/>`_

* https://source.chromium.org/chromium/chromium/src/+/master:chrome/common/pref_names.cc


Master preferences path
~~~~~~~~~~~~~~~~~~~~~~~

Master preferences path **can vary from one distribution to another**. Default
directory to place custom ``master_preferences`` file is ``base::DIR_EXE`` but
this can be changed by patching sources like this_:

.. code:: diff

    description: search for master_preferences in /usr/share/chromium
    author: Michael Gilbert <mgilbert (at) debian.org>

    --- a/chrome/browser/first_run/first_run_internal_linux.cc
    +++ b/chrome/browser/first_run/first_run_internal_linux.cc
    @@ -18,11 +18,7 @@ bool IsOrganicFirstRun() {
     }

     base::FilePath MasterPrefsPath() {
    -  // The standard location of the master prefs is next to the chrome binary.
    -  base::FilePath master_prefs;
    -  if (!base::PathService::Get(base::DIR_EXE, &master_prefs))
    -    return base::FilePath();
    -  return master_prefs.AppendASCII(installer::kDefaultMasterPrefs);
    +  return base::FilePath("/usr/share/chromium/master_preferences");
     }

     }  // namespace internal

.. _this: https://sources.debian.org/src/chromium-browser/70.0.3538.110-1~deb9u1/debian/patches/debianization/master-preferences.patch/

So, if the maintainer of your chromium package doesn't explicitly change master
preferences path, you should place ``master_preferences`` file into the chrome
executable directory, which can be determined by looking at **Executable
Path:** on the `chrome://version/`_ page. Just use dirname() on its value.

.. _`chrome://version/`: chrome://version/

TL;DR
`````
Depending on your distro you should use:

* on Arch Linux: ``/usr/lib/chromium/chromium/master_preferences``

* on Ubuntu: ``/usr/lib/chromium-browser/master_preferences``

* on Debian: ``/usr/share/chromium/master_preferences``

* on Windows: ``C:\Program Files (x86)\Google\Chrome\Application\master_preferences`` [#]_

* on OS X: ``~/Library/Application Support/Google/Chrome/Google Chrome Master``


Notes on further investigation
------------------------------

* `Chromium Code Search`_

* `Chrome Enterprise release notes`_

.. _`Chromium Code Search`: https://source.chromium.org/chromium
.. _`Chrome Enterprise release notes`: https://support.google.com/chrome/a/answer/7679408?hl=en
   
----

.. [#] https://blog.apnic.net/2020/08/21/chromiums-impact-on-root-dns-traffic/
.. [#] https://github.com/WICG/raw-sockets/blob/master/docs/explainer.md
.. [#] Note that `install.cmd` is designed for Google Chrome, not for Chromium.
       Also, browser and operating system architecture should be the same (you
       shouldn't use this script for x86 Chrome on x64 Windows).
.. [#] https://www.chromium.org/administrators/configuring-other-preferences
