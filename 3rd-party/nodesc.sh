#!/bin/sh
[ -z "$1" ] && echo "Missing argument: CSV file." >&2 && exit 1
awk -F"," '{print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12}' "$1"
