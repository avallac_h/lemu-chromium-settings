#!/usr/bin/env python3

import csv
import re

from collections import OrderedDict

DBG = 0

# RegExps
re_short_desc = re.compile(r"^(.*?[\.$])", re.DOTALL | re.MULTILINE)
re_whitespaces = re.compile(r"[\s\r\n]+", re.MULTILINE)
re_tags = re.compile(
    r"""
        <ph.*?>         # <ph name="PRODUCT_NAME">
        (?:\$\d+)*      # $1
        (?:<ex>)*       # <ex>
        (.*?)           # Google Chrome
        (?:</ex>)*      # </ex>
        </ph>           # </ph>
        """,
    re.VERBOSE | re.DOTALL | re.MULTILINE,
)


def shorten(description: str) -> str:
    try:
        return re.findall(re_short_desc, description)[0]
    except IndexError:
        return ""


def clean(s: str) -> str:
    s = re.sub(re_whitespaces, " ", s)
    return re.sub(re_tags, r"[\1]", s)


def makerow(d: dict, marker: str) -> list:
    if DBG > 1:
        print("-" * 72)

    row = []
    row.append(marker)
    row.append(d["name"])

    if d.get("deprecated"):
        row.append("D")
    else:
        row.append("")

    if d.get("features"):
        row.append("+" if d["features"].get("can_be_recommended") else "-")
        row.append("+" if d["features"].get("dynamic_refresh") else "-")
        row.append("+" if d["features"].get("per_profile") else "-")
    else:
        for i in range(3):
            row.append("")

    if d.get("supported_on"):
        support = OrderedDict(
            {"Win": "", "Mac": "", "Lin": "", "And": "", "CrOS": ""}
        )
        for i in d["supported_on"]:
            if i.startswith("chrome."):
                tos, ver = i.replace("chrome.", "").split(":")
                if tos == "*":
                    support["Win"] = ver
                    support["Mac"] = ver
                    support["Lin"] = ver
                else:
                    support[tos.title()[:3]] = ver
                if DBG > 1:
                    print(support)

            elif i.startswith("android"):
                support["And"] = i.split(":")[1]

            elif i.startswith("chrome_os"):
                support["CrOS"] = i.split(":")[1]

        for k, v in support.items():
            if DBG > 1:
                print("\t{}: {}".format(k, v))
            row.append(v.strip())
    else:
        for i in range(5):
            row.append("")

    if d.get("schema"):
        row.append(d["schema"].get("type", ""))
        row.append(d["schema"].get("enum", ""))
    else:
        for i in range(2):
            row.append("")

    row.append(d.get("example_value", ""))

    if d.get("caption"):
        row.append(clean(d["caption"]))

    if d.get("desc"):
        row.append(shorten(clean(d["desc"])))

    return row


def main():
    policies = eval(open("policy_templates.json", "r").read())
    definitions = policies["policy_definitions"]

    rows = []
    processed_as_group_member = []

    # header row
    header = [
        "#",
        "Name",
        "Depr",
        "Rec",
        "Dyn",
        "Pro",
        "Win",
        "Mac",
        "Lin",
        "And",
        "CrOS",
        "Type",
        "Vals",
        "Ex.",
        "Caption",
        "Description",
    ]
    rows.append(header)

    for d in definitions:

        if d["name"] in processed_as_group_member:
            continue

        if d["type"] == "group":
            # group of policy definitions ----------------------------------
            row = makerow(d, "[G]")
            rows.append(row)

            if DBG:
                print(row)

            for gm_name in d["policies"]:
                # group member (policy definition) -------------------------
                processed_as_group_member.append(gm_name)

                gm = [x for x in definitions if x["name"] == gm_name][0]

                if gm_name == d["policies"][-1]:
                    marker = " └─"
                else:
                    marker = " ├─"

                row = makerow(gm, marker)
                rows.append(row)

                if DBG:
                    print(row)

        else:
            # single policy definition --------------------------------------
            row = makerow(d, "[I]")
            rows.append(row)

            if DBG:
                print(row)

    with open("policy_templates.csv", "w") as fp:
        csv_writer = csv.writer(
            fp, delimiter=",", quotechar='"', lineterminator="\n"
        )
        for row in rows:
            if DBG > 2:
                print(row)
            assert len(row) == len(
                header
            ), "Wrong columns number, should be {}, got {}".format(
                len(row), len(header)
            )
            csv_writer.writerow(row)


if __name__ == "__main__":
    main()
